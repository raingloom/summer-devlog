# meta
this document serves as a journal of my progress

the source is https://github.com/raingloom/summer-devlog/

# first weeks

I mostly lurked on IRC/Zulip and tried to get more familiar with the search functions and tried to get a head-start on the programming part. I think I was supposed to ask more questions on IRC/Zulip but I didn't really know what to ask. On the other hand, I would have progressed faster if I did ask about certain things.I found a bug in the doxygen output, that produces incorrect XHTML. The fix _should_ be trivial but so far I haven't been able to find the setting or build step that causes it.

I also ran into a segmentation error when I was testing my changes to the search functions. I thought I messed something up but it turns out that MGED just doesn't handle that.

I was running into some build errors, which turned out to be because of some shorthand definitions that collided with a variable name I was using. I removed the definitions and wrote a simple Lua script to expand the definitions.

On that note: CMake is really not much help when you need to change compilation flags often.

Building the doxygen docs was a bit tricky, so I added notes about it to the wiki.I'm running into some build bug, someone changed a utility function's signature or something. If people would stop messing with parts of the code I have nothing to do with so I could keep hacking on src/librt/search.? that would just be. Great.

I'm staring at this and... idk, is this a compiler bug?

---

ok so, from now on, daily log, collected in this file

not weekly, in their own files

---

# log

Wed May 16 14:11:41 CES 2018
It was a compiler bug. What the hell GCC?
And why can't I build MGED with Clang? ;_;

RETCON: it wasn't a bug exactly, but it was annoying

OK, so, back to pointer hell.

Nvm pointer hell, gotta do more org related things.

TIL Acme globbing is weird, `cat *.md > index.md` generates an infinite index.md, because glob expansion happens after creation of index.md

Wed May 16 18:15:34 CES 2018
ok I _think_ I have all the bureaucracy/org stuff done??? idk, I just wanna continue writing code

btw I'm writing this document with Acme

Wed May 16 21:51:00 CES 2018
YAAAY
finally
my command shim doesn't crash or print any errors

uhm. why is c_exec called like, 8 times or whatever, what's going on

i'm incrementing *argvp properly, aren't i?

Thu May 17 02:08:53 CES 2018

well, it wasn't my fault apparently, it just... does that.... for some weird reason

damn that's ugly

I'm adding this whole thing to my "this wouldn't have happenned on Plan 9" list

it's.... a very long list

I should stop being surprised when this code does something surprising and just remind myself that "yep, the rest of it is probably just as ugly, but apparently it still functions"

but seriously, why did it parse the arguments 8 times

EIGHT. TIMES.

WHY.

and it's not just my code apparently, I stuck a static counter into c_name and that did the same thing

ok, time to wrap up for today at Thu May 17 03:17:01 CES 2018

Thu May 17 18:36:02 CES 2018
tested gcc 8 again, there still are errors

I'm trying to put proper error handling in there but.... meh, if it prints the same error 8 times.... that's still not terrible. Oh how I miss Option<T>. Maybe I'll have to add an error string somewhere.

So, today I added the callback type and looked around for how the other nodes do evaluation.

Fri May 18 17:02:48 CES 2018
new day! full of opportunities!

I need to get in another patch to get commit acces so... why not go back to that doxygen thing? And solve that? ...eh, nevermind, I have more C stuff to do. Let's write the f_exec evaluator thingy, because that doesn't change the db_search interface just yet.

ok, back home. so, there is that _e_argv field in the plan node struct, but idk what it's useful for, I'll leave it there for now but idk if it's actually useful, since it changes each time in f_exec.

oh, I get it why. _e_argv points to the same strings as _e_orig and we just change the {} ones during evaluation.

Sat May 19 16:00:14 CES 2018
so, things build nicely now, shoutout to Sean! let's write f_exec and modify c_exec to fit that.

This indentation style is so weird. It almost makes me regret using Acme.
I think I'll have to install Emacs again to use the auto-indent script.

So, how do we make strings from the paths? struct db_node_t seems to have two kinds of path, so we should support both? I think? So there should be two kinds of holes.... oh yes, I started calling the '{}' thing a "hole".

If I had more time I could write a script that included my commit messages in this thing, but... eh. Maybe I could do it with git hooks? Putting it in TODO.

Ok, so now I should look into how to write test because I said I'd do TDD but oh my stars, CMake is horrible and I have no idea how testing works.

Ok, so now I have the f_exec function and exec plans are propely freed now.

Soo, I should leave db_search alone until I have everything together??? Or I could write the ged side of things? :/

And hook things up later??

Eh. I'll change db_search first and then do a big ol' refactor on like 200 gajillion calls to it. That'll be fun. Probably should leave it for tomorrow though, it's already late. (Sun May 20 01:27:34 CES 2018 to be exact)

Btw the way I do these is by writing "<date" and then selecting it and then middle clicking it. The power of Acme ;)

Mon May 21 10:38:38 CES 2018
yay, fresh day, full of opportunities. let's fetch the latest commits.

ahhh, the C++ devs are up to their old hijinks again, those silly kids.

just a sampling of the.... **several** errors I'm getting:

```
In file included from /home/rain/Sync/gsoc/brlcad-code/include/./rt/func.h:42,
                 from /home/rain/Sync/gsoc/brlcad-code/include/raytrace.h:120,
                 from /home/rain/Sync/gsoc/brlcad-code/src/librt/primitives/table.cpp:39:
/home/rain/Sync/gsoc/brlcad-code/src/librt/primitives/table.cpp:227:30: error: cast between incompatible function types from ‘int (*)(rt_db_internal*, const bu_external*, const fastf_t*, const db_i*)’ {aka ‘int (*)(rt_db_internal*, const bu_external*, const double*, const db_i*)’} to ‘int (*)(rt_db_internal*, const bu_external*, const fastf_t*, const db_i*, resource*)’ {aka ‘int (*)(rt_db_internal*, const bu_external*, const double*, const db_i*, resource*)’} [-Werror=cast-function-type]
  RTFUNCTAB_FUNC_IMPORT5_CAST(rt_tor_import5),
                              ^~~~~~~~~~~~~~
/home/rain/Sync/gsoc/brlcad-code/include/rt/functab.h:165:160: note: in definition of macro ‘RTFUNCTAB_FUNC_IMPORT5_CAST’
 #define RTFUNCTAB_FUNC_IMPORT5_CAST(_func) ((int (*)(struct rt_db_internal *, const struct bu_external *, const mat_t, const struct db_i *, struct resource *))_func)
                                                                                                                                                                ^~~~~
```

to the chats I guess and also let's roll back that last merge >_>

rollback didn't help so it must be a new GCC check again

rolled back the GCC package again

ok, doing the TCL eval now

is that how you write TCL? or is it Tcl? as a Lua user I know how irritating it is when people write the name of your language of choice with the wrong capitalization.

ok, it's Tcl

well, the Tcl C API docs are... not very good... at least so far. Lua's docs are so much better. I guess I'm spoiled. Also, there was no Lua in the 80's. Heck, not even in the 90's, not in its modern state at least.

UUUh. Which of these lets me call a function with a list of parameters???

On that note, you know what would be cool? Implementing MGED databases as 9P fileservers!

Tue May 22 16:41:10 CES 2018
gonna write tests today because yeah, tests are important

ok yeah no, I gotta write that not printing thing... but that's um, that's only relevant in MGED. hmm.

Wed May 23 21:27:53 CES 2018
I took sime me-time today because things were going bad. Helped out in the garden and such. Gotta reset my circadian rhythm.

But tomorrow is gonna be a busy day.

Anyways, I went over my plan and it turns out I have Milestone 0 ready??? But I wish I could work faster, so I'm gonna rest up today and get cracking on the MGED stuff tomorrow.

Kids, don't stay up late or you'll have headaches. Take it from me.

Fri May 25 00:58:17 CES 2018
Ok let's do some programmering. I'm gonna leave the no-implicit-printing thing for later, doing Tcl eval now.

So, Tcl has no booleans. How quaint. Not even in return values. Well... we could do some conversion from those weird string things but... nah. Let's just expect "0" for false and anything else numeric for true.

Hmm. I still miss Lua.

Sat May 26 18:40:33 CES 2018
Ugh, busy week, had a lot of IRL stuff going on, so gonna do some catching up.

Ok, I've looked at the libged commands and I don't think it would make sense to write an exec evaluator for it, since they are not used on their own.

ok, changed a bunch of function signatures in librt search, let's start a build and see what breaks

btw you know what would be a good build system for this situation? tup

oh damn, it's morning. I was up refactoring for.... a lot longer than I wanted to be.

annnnnnnnnnd it builds again. well, librt builds again. I'm sure there a bunch of new errors elsewhere. that's for tomorrow.

this was tiring, but satisfying.

Sun May 27 18:08:56 CES 2018
So, let's get MGED to compile and see what we can do about that Tcl eval thing

Tue May 29 20:17:19 CES 2018
Last few days were pretty meh in terms of progress, libtclcad is not as forgiving about changing function types.

Sat Jun  2 02:42:17 CES 2018
Ok let's get back on track, irl was a bit hectic. So, let's rewrite things to use a context.

Mmm, ok, maybe I shouldn't just put in a bunch of NULL pointers to the existing db_search calls, because what if they _might_ use -exec???? Let's go over them one by one I guess.

Sun Jun  3 00:30:42 CES 2018
Well, I fell asleep yesterday. Let's continue where we left off, ie.: seeing where db_search is called and how to make those calls work again AND!!! make them hard to break later. We don't wanna stick a NULL pointer in everywhere because it's not impossible that one of them could call with -exec.

I wonder if NULLs are good here?? Like, I could put in proper contexts but it's not going to be used anyway??? If someone changes these calls to use exec, it will error quite early and they'll be able to track it down. Plus this is still the iterating phase, so let's not get carried away.

I wonder if I should have added the new parameter before the instance pointer???... I probably should have.... but this was easier to write... I guess I'll rewrite it later. Let's just get it working for now.

Ok, so the eval callback might not be a static function, so let's write a function that returns an appropriate evaler for a gedp.

Which for now will only return a pointer from the struct ged.

ok, let's sleep a bit coz it's 3 am

Sun Jun  3 13:55:31 CES 2018
Goood morning devlog! Where did we leave off? Ah yes. Evaler.

So, I looked at where Tcl is used right now and it turns out there is already an eval function and libged already depends on Tcl so I guess I'm putting the callback there.

Tue Jun  5 00:38:34 CES 2018
Ok, let's get a working prototype today! Yesterday I wrote most of the implementation part now I just have to get it linked and have the right includes. Which means........CMake. We meet again.

Or... I could just write the same function? It's a private one so uh.. not sure how to make it private nicely??

Eh, it's just one function, let's reimplement it for now and make a FIXME note saying what the original is.

oh kayyy mged also builds but I know that it will crash because the callback pointer is not initialized anywhere

hecc, missing Rust here a lot, it would not have let my compile something with uninitialized pointers.

ok, let's see where ged_interp is assigned and assign the callback there.

Tue Jun  5 19:35:04 CES 2018
aaaaaaaaaaaaaaaaaAA!!!.....

....

AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

```
% echo 'search -exec ";"' | ./bin/mged ../../db/axis.g
yay
```

IT DID SOMETHING!!!!

ok, invalid commands terminate it.... but why is it called only once... let's see what the internals look like

Thu Jun  7 04:25:42 CES 2018
I've been staring at this and idk what's going on. Need to call in Debug Bear.

Thu Jun  7 21:58:03 CES 2018
Debug Bear was no help but I explained the thingy with the output to a friend and I realized it was all Tcl's fault. Thank you, Tcl. Lua is still better.